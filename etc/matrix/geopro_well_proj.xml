<?xml version="1.0" encoding="utf-8"?>
<!--
  ~ Copyright (c) 2018. Navilab.
  ~ THE SOFTWARE IS PROVIDED "AS IS", WITHOUT ANY RESPONSIBILITY OF THE AUTHOR.
  -->

<define_entities>
    <entity name="WellProj">
        <fields>
            <field name="ID_DOC" type="INT" filter-sql-name="docname-filter">
                <dict entity="ProjDocDict" field_code="ID_DOC" field_name="DOC_NAME"/>
            </field>
            <field name="OPERATOR" type="INT" filter-sql-name="operator-filter">
                <dict entity="GeoproDictGNgdu" field_code="ID" field_name="DESCRIPTION"/>
            </field>
            <field name="FIELD" type="STRING" filter-sql-name="field-filter">
                <dict entity="GeoproDictFieldCode" field_code="FIELD_CODE" field_name="FIELD_NAME"/>
            </field>
            <field name="OBJECT_ID" type="INT" filter-sql-name="object-id-filter">
                <dict entity="DictReservoirs" field_code="ID_DO" field_name="OBJECT_NAME"/>
            </field>
            <field name="PROEKTN_NAZ" type="INT">
                <dict entity="ProdClassDict" field_code="ID" field_name="DESCRIPTION"/>
            </field>
            <field name="WELL_PROJECT_NUMBER" type="STRING"/>
            <field name="UWI" type="STRING"/>
            <field name="X_PERES_HORIZ_STV" type="FLOAT"/>
            <field name="Y_PERES_HORZ_STV" type="FLOAT"/>
            <field name="X_ZAB" type="FLOAT"/>
            <field name="Y_ZAB" type="FLOAT"/>
            <field name="ZAV" type="STRING"/>
            <field name="PROGNOZ_NEF" type="FLOAT"/>
            <field name="DOC_DATE" type="DATE"/>
            <field name="DOC_NUM" type="STRING"/>
            <field name="STATUS" type="INT"/>
            <field name="COMMENTS" type="STRING"/>
            <field name="LAST_UPDATE" type="DATE"/>
            <field name="INSERT_DATE" type="DATE"/>
            <field name="UPDATED_BY" type="STRING"/>
            <field name="INSERTED_BY" type="STRING"/>
            <field name="OBJECTID" type="INT"/>
            <field name="LEASE_NO" type="INT">
                <dict entity="LeaseDict" field_code="ID" field_name="DESCRIPTION"/>
            </field>
            <field name="LAYER_ID" type="INT">
                <dict entity="DictLayersEst" field_code="LAYER_ID" field_name="LAYER_TRUE"/>
            </field>
            <field name="DEPOSIT_ID" type="INT">
                <dict entity="DictDeposits" field_code="DEPOSIT_ID" field_name="DEPOSIT_NAME_GOSBALANCE"/>
            </field>
            <field name="SHAPE" type="OBJECT"/>
        </fields>
        <sql>
            SELECT {FIELDS} FROM {DEFAULT_PROJECT}.well_proj_line
            UNION ALL SELECT {FIELDS}
            FROM {DEFAULT_PROJECT}.well_proj_point
        </sql>
        <sql name="filtered">
            SELECT * FROM (SELECT {FIELDS} FROM {DEFAULT_PROJECT}.well_proj_line
            UNION ALL SELECT {FIELDS} FROM nipi42.well_proj_point) WHERE {WHERE_CONDITION}
            ORDER BY operator, field
        </sql>
        <sql name="field-filter">
            SELECT DISTINCT field as id,
            (SELECT DISTINCT description from codes.dict_g where grp=1000001 and code = t.field) as description
            FROM (SELECT * FROM NIPI42.well_proj_line
            UNION ALL SELECT * FROM NIPI42.well_proj_point) t
            WHERE {WHERE_CONDITION}
            ORDER BY description
        </sql>
        <sql name="operator-filter">
            SELECT DISTINCT operator as id,
            (SELECT description FROM codes.dict_g where id=t.operator ) as description
            FROM ( SELECT * FROM nipi42.well_proj_line
            UNION ALL SELECT * FROM nipi42.well_proj_point ) t
            WHERE {WHERE_CONDITION}
            ORDER BY description
        </sql>
        <sql name="docname-filter">
            SELECT DISTINCT id_doc AS id,
            (SELECT doc_name || ' ' || EXTRACT(YEAR FROM protocol_date) doc_name FROM codes.dict_proj_doc
            WHERE id_doc = t.id_doc) AS description
            FROM ( SELECT * FROM nipi42.well_proj_line
            UNION ALL SELECT * FROM nipi42.well_proj_point) t
            WHERE {WHERE_CONDITION}
            ORDER BY description
        </sql>
        <sql name="object-id-filter">
            SELECT DISTINCT object_id as id,
            (SELECT (SELECT description FROM codes.dict_g dg WHERE d.field = dg.code and grp = 1000001)
            || '-' || object_name
            FROM codes.dict_reservoir d
            WHERE t.object_id = id_do) as description
            FROM ( SELECT * FROM nipi42.well_proj_line
            UNION ALL SELECT * FROM nipi42.well_proj_point) t
            WHERE {WHERE_CONDITION}
            ORDER BY description
        </sql>
    </entity>

    <entity name="WellProjPR">
        <fields>
            <field name="AREA" type="INT">
                <dict entity="GeoproDictArea" field_code="ID" field_name="DESCRIPTION"/>
            </field>
            <field name="REGION" type="INT">
                <dict entity="GeoproDictRegion" field_code="ID" field_name="DESCRIPTION"/>
            </field>
            <field name="LIC_ID" type="INT" filter-sql-name="lic-filter">
                <dict entity="LeaseDict" field_code="ID" field_name="DESCRIPTION"/>
            </field>
            <field name="FIELD" type="STRING" filter-sql-name="field-filter">
                <dict entity="GeoproDictFieldCode" field_code="FIELD_CODE" field_name="FIELD_NAME"/>
            </field>
            <field name="FLDPOOLWILD" type="INT" filter-sql-name="fldpoolwild-filter">
                <dict entity="FldPoolDict" field_code="ID" field_name="DESCRIPTION"/>
            </field>
            <field name="WELL_NAME_ORIG" type="STRING"/>
            <field name="WELL_NAME_LAT" type="STRING"/>
            <field name="DRILL_YEAR" type="INT"/>
            <field name="STATUS" type="INT" filter-sql-name="status-filter">
                <dict entity="WellConditionDict" field_code="ID" field_name="DESCRIPTION"/>
            </field>
            <field name="UWI" type="STRING"/>
            <field name="FLAG_DEP" type="INT"/>
            <field name="DRILL_CLASS" type="INT">
                <dict entity="ProdClassDict" field_code="ID" field_name="DESCRIPTION"/>
            </field>
            <field name="NODE_X" type="FLOAT"/>
            <field name="NODE_Y" type="FLOAT"/>
            <field name="LAYER_PROJ_ID" type="INT"/>
            <field name="LAYER_PROJ_NAME" type="STRING"/>
            <field name="DEPTH_PROJ" type="INT"/>
            <field name="DATE_START" type="DATE"/>
            <field name="DOC1_ID" type="INT">
                <dict entity="GeoproDictDoc" field_code="DOC_ID" field_name="DOC_NAME"/>
            </field>
            <field name="DOC2_ID" type="INT">
                <dict entity="GeoproDictDoc" field_code="DOC_ID" field_name="DOC_NAME"/>
            </field>
            <field name="DOC3_ID" type="INT">
                <dict entity="GeoproDictDoc" field_code="DOC_ID" field_name="DOC_NAME"/>
            </field>
            <field name="COMMENTS" type="STRING"/>
            <field name="SHAPE" type="OBJECT"/>
            <field name="INSERT_DATE" type="DATE"/>
            <field name="INSERT_USER" type="STRING"/>
            <field name="UPDATE_DATE" type="DATE"/>
            <field name="UPDATE_USER" type="STRING"/>
        </fields>
        <sql>
            SELECT {FIELDS} FROM {DEFAULT_PROJECT}.well_proj_p_r
        </sql>
        <sql name="filtered">
            SELECT {FIELDS}
            FROM {DEFAULT_PROJECT}.well_proj_p_r
            WHERE {WHERE_CONDITION} ORDER BY FIELD
        </sql>
        <sql name="lic-filter">
            SELECT DISTINCT lic_id as id,
            (SELECT description FROM codes.dict_g WHERE id=t.lic_id) as description
            FROM {DEFAULT_PROJECT}.well_proj_p_r t WHERE {WHERE_CONDITION}
            ORDER BY description
        </sql>
        <sql name="field-filter">
            SELECT DISTINCT field as id,
            (SELECT field_name FROM {DEFAULT_PROJECT}.field_hdr p WHERE p.field_code = t.field) as description
            FROM {DEFAULT_PROJECT}.well_proj_p_r t WHERE {WHERE_CONDITION}
            ORDER BY description
        </sql>
        <sql name="fldpoolwild-filter">
            SELECT DISTINCT FLDPOOLWILD as id,
            (SELECT description FROM {DEFAULT_PROJECT}.dict_g p WHERE p.id = t.FLDPOOLWILD) as description
            FROM {DEFAULT_PROJECT}.well_proj_p_r t WHERE {WHERE_CONDITION}
            ORDER BY description
        </sql>
        <sql name="status-filter">
            SELECT DISTINCT status as id,
            (SELECT description FROM {DEFAULT_PROJECT}.dict_g p WHERE p.id = t.status ) as description
            FROM {DEFAULT_PROJECT}.well_proj_p_r t WHERE {WHERE_CONDITION}
            ORDER BY 2
        </sql>
    </entity>

</define_entities>
