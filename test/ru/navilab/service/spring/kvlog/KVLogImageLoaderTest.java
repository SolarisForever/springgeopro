package ru.navilab.service.spring.kvlog;

import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;

import static org.junit.Assert.*;

public class KVLogImageLoaderTest {
    public static void main(String[] args) throws Throwable {
        JFrame frame = new JFrame();
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        BufferedImage img = KVLogImageLoader.load("nipi42", "40015497");
        JPanel view = new JPanel() {
            @Override
            public void paint(Graphics g) {
                super.paint(g);
                g.drawImage(img, 0, 0, null);
            }
        };
        view.setPreferredSize(new Dimension(img.getWidth(), img.getHeight()));
        frame.setContentPane(new JScrollPane(view));
        frame.setPreferredSize(new Dimension(800, 600));
        frame.pack();
        frame.setVisible(true);
    }
}