#!/bin/sh

# 15.05.2020 RomanovEV3
# JAXRS service start script
JAVA_HOME=$HOME/jdk1.8.0_241/jre; export JAVA_HOME
PID_FILE=spring-geopro.pid

case "$1" in
 
'start')
  nohup ${JAVA_HOME}/bin/java -Djava.util.logging.config.file=logging.properties -cp "spring-geopro.jar:lib/*" ru.navilab.service.spring.Application > spring-geopro.log 2>&1 &
  pid=$!
  echo $pid > ${PID_FILE}
  ;;

'stop')
  kill `cat ${PID_FILE}`
  rm ${PID_FILE} 
  ;;
*)
  echo "Usage: $0 { start | stop }"
  ;;
esac
exit 0
