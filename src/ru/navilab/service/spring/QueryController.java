package ru.navilab.service.spring;

import org.apache.catalina.connector.Connector;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.embedded.tomcat.TomcatConnectorCustomizer;
import org.springframework.boot.web.embedded.tomcat.TomcatServletWebServerFactory;
import org.springframework.boot.web.servlet.server.ConfigurableServletWebServerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import ru.navilab.service.spring.geopro.GeoproDataLoader;
import ru.navilab.service.spring.geoserver.GeoserverLoader;
import ru.navilab.service.spring.geoserver.GeoserverException;
import ru.navilab.service.spring.kvlog.KVLogImageLoader;

import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletRequest;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.util.*;

@RestController
public class QueryController {
    @Value("${geoserver.url}")
    private String geoserverUrl;

    @CrossOrigin(origins = "*")
    @RequestMapping("/geopro/query")
    public List<Map<String, String>> query(@RequestParam(value="entity") String entity, @RequestParam(value="sql", required = false) String sql, @RequestParam(value = "bind", required = false) String[] bind) {
        try {
            return GeoproDataLoader.load(entity, sql, bind);
        }
        catch (Exception e){
            e.printStackTrace();
        }
        return null;
    }

    @RequestMapping(value = "/kvlog/{project}/{uwi}", method = RequestMethod.GET, produces = MediaType.IMAGE_JPEG_VALUE)
    public byte[] kvlog(@PathVariable("project") String project, @PathVariable("uwi") String uwi) {
        try {
            BufferedImage img = KVLogImageLoader.load(project, uwi);
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            ImageIO.write( img, "jpg", baos );
            baos.flush();
            return baos.toByteArray();
        }
        catch (Throwable e){
            e.printStackTrace();
        }
        return null;
    }

    @RequestMapping(value = "/geoserver", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public String geoserver(HttpServletRequest request) throws GeoserverException {
        Map<String, String[]> parameterMap = request.getParameterMap();
        return GeoserverLoader.geoserverRequest(geoserverUrl, parameterMap);
    }

    @Bean
    public ConfigurableServletWebServerFactory webServerFactory() {
        TomcatServletWebServerFactory factory = new TomcatServletWebServerFactory();
        factory.addConnectorCustomizers(new TomcatConnectorCustomizer() {
            @Override
            public void customize(Connector connector) {
                connector.setProperty("relaxedQueryChars", "|{}[]");
            }
        });
        return factory;
    }

}

