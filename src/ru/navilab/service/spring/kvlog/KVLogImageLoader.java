package ru.navilab.service.spring.kvlog;

import navi.client.dao.CurveDelegate;
import navi.client.dao.WellHdrDelegate;
import navi.client.dao.oldadapters.IOldWellHdr;
import navi.client.geo.curve.KVLogData;
import navi.client.geo.kvlog.KVLogDataLoader;
import navi.client.geo.kvlog.KVLogPlot;
import navi.client.geo.kvlog.LogDataLoadException;
import navi.client.geo.kvlog.NaviKVLogModelBuilder;
import navi.client.matrix.adapter.*;
import org.json.simple.parser.ParseException;
import ru.navilab.common.dao.DataAccessException;
import ru.navilab.common.data.filter.DistinctArrayList;
import ru.navilab.common.ui.progress.ProgressCancelException;
import ru.navilab.kvlog.j2d.MainView;
import ru.navilab.kvlog.model.LogPlotModel;
import ru.navilab.kvlog.utils.IFloatBounds;
import ru.navilab.kvlog.view.ViewContext;
import ru.navilab.matrix.MatrixConfigurationException;
import ru.navilab.matrix.engine.Matrix;
import ru.navilab.matrix.load.MatrixSelectHelper;

import java.awt.image.BufferedImage;
import java.sql.SQLException;
import java.util.*;
import java.util.List;
import java.util.logging.Logger;

public class KVLogImageLoader {
    private static String[] getFieldCodeArray(String[] uwiArray, String project) {
        WellHdrDelegate wellHdrDelegate = null;
        try {
            wellHdrDelegate = WellHdrDelegate.getInstance();
            List<IOldWellHdr> wellHdrList = wellHdrDelegate.getByUwis(uwiArray, project);
            List<String> list = new DistinctArrayList<>();
            for (IOldWellHdr wellHdr : wellHdrList) {
                list.add(wellHdr.getField());
            }
            String[] buffer = new String[list.size()];
            list.toArray(buffer);
            return buffer;
        } catch (DataAccessException e) {
            e.printStackTrace();
        }
        return null;
    }

    private static float getSaturationTopValue(KVLogData kvLogData, float offset, float defaultValue) {
        Matrix saturationMatrix = kvLogData.getSaturationMatrix();
        List<IWellLogResultSublayers> list = MatrixSelectHelper.getList(saturationMatrix, WellLogResultSublayersAdapter.createFactory());
        float top = Integer.MAX_VALUE;
        for (IWellLogResultSublayers sublayer : list) {
            if (sublayer.getTop() != null) {
                top = Math.min(sublayer.getTop(), top);
            }
        }
        if (top == Integer.MAX_VALUE) {
            return defaultValue;
        }
        return top + offset;
    }


    public static BufferedImage load(String project, String uwi) throws DataAccessException, ProgressCancelException, LogDataLoadException, MatrixConfigurationException, ParseException, SQLException {
        Logger logger = Logger.getLogger(KVLogImageLoader.class.getName());
        logger.info(String.format("LOAD KVLOG project:%s uwi:%s", project, uwi));
        final KVLogPlot logPlot = KVLogPlot.getInstance();
        KVLogDataLoader dataLoader = new KVLogDataLoader();
        logger.fine("load curve data");
        Matrix curveMatrix = CurveDelegate.getInstance().getCurveMatrix(Entities.GEOPRO_CURVE.getEntityName(), uwi, project);
        List<IGeoproCurve> allCurveList = MatrixSelectHelper.getList(curveMatrix, GeoproCurveAdapter.createFactory());
        dataLoader.setFieldCodeArray(getFieldCodeArray(new String[]{uwi}, project));
        logger.fine("load kvlog data");
        final KVLogData logData = dataLoader.loadData(uwi, project, Collections.EMPTY_LIST, allCurveList);
        final NaviKVLogModelBuilder modelBuilder = new NaviKVLogModelBuilder(logPlot.getCurrentLogPattern().getBody());
        LogPlotModel logPlotModel = modelBuilder.buildModel(logData);

        ViewContext context = new ViewContext(logPlotModel);
        MainView mainView = new MainView(context);

        IFloatBounds depthBounds = logPlotModel.getDepthBounds();
        float startDepth = getSaturationTopValue(logData, -20f, depthBounds.getMin());

        logger.fine("print image start");
        BufferedImage bufferedImage = mainView.printImage(startDepth);
        logger.fine("print image end");

        return bufferedImage;
    }
}
