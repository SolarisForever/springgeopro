package ru.navilab.service.spring.geopro;

import navi.client.matrix.NaviMatrixConfiguration;
import navi.client.matrix.NaviMatrixEngine;
import ru.navilab.common.dao.DataAccessException;
import ru.navilab.matrix.BindKey;
import ru.navilab.matrix.BindParameterBuilder;
import ru.navilab.matrix.MatrixConfigurationException;
import ru.navilab.matrix.engine.Matrix;
import ru.navilab.matrix.engine.MatrixCell;
import ru.navilab.matrix.engine.MatrixRow;
import ru.navilab.matrix.entity.MatrixEntity;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class GeoproDataLoader {
    private static Pattern bindPattern = Pattern.compile("\\{(.*?)\\}");

    public static List<Map<String, String>> load(String entity, String sql, String[] bind) throws Exception {
        Logger logger = Logger.getLogger(GeoproDataLoader.class.getName());
        logger.info(String.format("LOAD entity:%s sql%s", entity, sql));
        BindParameterBuilder builder = createBindParameterBuilder(sql, bind);
        Matrix matrix = loadMatrix(entity, builder);
        return matrixToMap(matrix);
    }

    private static BindParameterBuilder createBindParameterBuilder(String sql, String[] bindArray) throws Exception {
        BindParameterBuilder builder = new BindParameterBuilder();
        if (sql != null && sql.length() > 0) {
            builder.setSqlName(sql);
        }
        if (bindArray != null) {
            for (String bind : bindArray) {
                Matcher matcher = bindPattern.matcher(bind);
                if (matcher.find()) {
                    String key = matcher.group(1);
                    String value = bind.substring(matcher.end());
                    builder.addKey(new BindKey(key, value, false));
                } else {
                    throw new Exception("Wrong bind key format: " + bind + ". Supported format: {KEY}VALUE");
                }
            }
        }
        return builder;
    }

    private static Matrix loadMatrix(String entityName, BindParameterBuilder bindParameterBuilder) throws SQLException, MatrixConfigurationException, DataAccessException {
        NaviMatrixConfiguration matrixConfiguration = NaviMatrixConfiguration.getInstance();
        NaviMatrixEngine matrixEngine = NaviMatrixEngine.getInstance();
        MatrixEntity entity = matrixConfiguration.getEntity(entityName);
        return matrixEngine.load(entity, bindParameterBuilder.createBindParameter());
    }

    private static List<Map<String, String>> matrixToMap(Matrix matrix) {
        List<Map<String, String>> result = new ArrayList<>();
        int rowCount = matrix.getRowCount();
        int columnCount = matrix.getColumnCount();
        for (int i = 0; i < rowCount; i++) {
            MatrixRow row = matrix.getRow(i);
            Map<String, String> rowMap = new HashMap<>();
            for (int j = 0; j < columnCount; j++) {
                MatrixCell cell = row.getCell(j);
                String columnName = cell.getColumnName();
                Object value = cell.getObjectValue();
                if (value != null) {
                    rowMap.put(columnName, value.toString());
                }
            }
            result.add(rowMap);
        }
        return result;
    }
}
