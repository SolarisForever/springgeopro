package ru.navilab.service.spring.geoserver;

public class GeoserverException extends Throwable {
    public GeoserverException(String message) {
        super(message);
    }
}
