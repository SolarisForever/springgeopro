package ru.navilab.service.spring.geoserver;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import org.json.simple.JSONObject;

import java.util.Map;
import java.util.Set;

public class GeoserverLoader {
    public static String geoserverRequest(String url, Map<String, String[]> parameterMap) throws GeoserverException {
        Client client = Client.create();
        WebResource webResource = client.resource(url);
        Set<Map.Entry<String, String[]>> entries = parameterMap.entrySet();
        for (Map.Entry<String, String[]> entry : entries) {
            webResource = webResource.queryParam(entry.getKey(), entry.getValue()[0]);
        }
        ClientResponse response = webResource.get(ClientResponse.class);
        if(response.getStatus()!=200){
            throw new GeoserverException(response.toString());
        }
        return response.getEntity(String.class);
    }
}
